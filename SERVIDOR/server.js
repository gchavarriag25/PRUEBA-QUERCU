const express = require("express");

require("dotenv").config({ path: "variables.env" });

const cors = require("cors");

const app = express();

const db = require("./models");

db.sequelize.sync();

// app.use(cors(corsOptions));
app.use(cors());
// parse requests of content-type - application/json
app.use(express.json({limit: '20000kb'}));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));


// Rutas
require("./routes/propertytype")(app);
require("./routes/owner")(app);
require("./routes/property")(app);


// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Servidor corriendo en puerto: ${PORT}`);
});

