module.exports = (app) => {
  const property = require("../controllers/property");
  const { check } = require('express-validator');
  const {validarbody} = require('../midleware');
  const router = require("express").Router();

  /**Crear */
  router.post("/",[
  check('number', 'El numero es obligatorio').not().isEmpty(),
  check('address', 'La direccion es obligatoria').not().isEmpty(),
  validarbody,
  check('area', 'El area es obligatoria').not().isDecimal()
  ], property.crear);

  /**Obtener */
  router.get("/",property.obtener);

  router.get("/data", property.dataProperty);

  /**Actualizar */
  router.put("/:id", 
  [
    check('number', 'El numero es obligatorio').not().isEmpty(),
    check('address', 'La direccion es obligatoria').not().isEmpty(),
    validarbody,
    check('area', 'El area es obligatoria').not().isDecimal()
  ],property.actualizar);

  /**Eliminar */
  router.delete("/:id", property.eliminar);

  app.use("/api/property", router);
};
