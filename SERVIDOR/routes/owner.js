module.exports = (app) => {
  const owner = require("../controllers/owner");
  const { check } = require('express-validator');
  const {validarbody} = require('../midleware');
  const router = require("express").Router();

  /**Crear */
  router.post("/",[
  check('name', 'El nombre es obligatorio').not().isEmpty(),
  check('telephone', 'El telefono es obligatorio').not().isEmpty(),
  check('identificactionnumber', 'El numero de identificacion es obligatorio').not().isEmpty(),
  validarbody
  ], owner.crear);

  /**Obtener */
  router.get("/",owner.obtener);

  /**Actualizar */
  router.put("/:id", 
  [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('telephone', 'El telefono es obligatorio').not().isEmpty(),
    check('identificactionnumber', 'El numero de identificacion es obligatorio').not().isEmpty(),
    validarbody
  ],owner.actualizar);

  /**Eliminar */
  router.delete("/:id", owner.eliminar);
 
  app.use("/api/owner", router);
};
