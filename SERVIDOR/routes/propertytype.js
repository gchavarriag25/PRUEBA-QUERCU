module.exports = (app) => {
  const propertytype = require("../controllers/propertytype");
  const { check } = require('express-validator');
  const {validarbody} = require('../midleware');

  const router = require("express").Router();

  /**Crear */
  router.post("/",[
    check('description', 'La descripción es obligatoria').not().isEmpty(),
    validarbody
  ], propertytype.crear);

  /**Obtener */
  router.get("/",propertytype.obtener);

  /**Actualizar */
  router.put("/:id", [
    check('description', 'La descripción es obligatoria').not().isEmpty(),
    validarbody
  ],propertytype.actualizar);

  /**Eliminar */
  router.delete("/:id", propertytype.eliminar);
 
  app.use("/api/propertytype", router);
};
