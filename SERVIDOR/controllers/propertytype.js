let models = require("../models");

/**Crear tipo de propiedad */
exports.crear = async (req, res) => {
  let data = req.body;
  try {
    await models.PropertyTypes.create(data);
    res.status(201).json("Tipo de propiedad creada con éxito.")
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener tipos de propiedades */
exports.obtener = async (req, res) => {
  try {
    const propertys = await models.PropertyTypes.findAll({attributes: ["id","description"],order: [['description', 'ASC']],raw: true});
    res.status(200).json(propertys)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Actualizar tipo de propiedad */
exports.actualizar = async (req, res) => {
  const {id} = req.params;
  let data = req.body; 
  try {
    await models.PropertyTypes.update(data, {where: { id }});    
    res.status(200).json("Tipo de propiedad actualizada con éxito.");
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Eliminar tipo de propiedad */
exports.eliminar = async (req, res) => {
  const {id} = req.params;
  try {
    await models.PropertyTypes.destroy({where: { id }});    
    res.status(200).json("Tipo de propiedad Eliminada con éxito.");
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};