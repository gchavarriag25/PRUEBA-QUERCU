let models = require("../models");

/**Crear propiedad */
exports.crear = async (req, res) => {
  let data = req.body;
  try {
    await models.Propertys.create(data);
    res.status(201).json("Propiedad creada con éxito.")
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener propiedades */
exports.obtener = async (req, res) => {
  try {
    const propertys = await models.Propertys.getPropertys();
    res.status(200).json(propertys)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Actualizar propiedad */
exports.actualizar = async (req, res) => {
  const {id} = req.params;
  let data = req.body; 
  try {
    await models.Propertys.update(data, {where: { id }});    
    res.json("Propiedad actualizada con éxito.");
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Eliminar propiedad */
exports.eliminar = async (req, res) => {
  const {id} = req.params;
  try {
    await models.Propertys.destroy({where: { id }});    
    res.status(200).json("Propiedad eliminada con éxito.");
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener data crear propiedad */
exports.dataProperty = async (req, res) => {
  try {
    const propertyTypes = await models.Propertys.getPropertyTypes();
    const owners = await models.Propertys.getOwners();

    res.status(200).json({propertyType: propertyTypes, owners: owners});
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};