let models = require("../models");

/**Crear dueño */
exports.crear = async (req, res) => {
  let data = req.body;
  try {
    await models.Owners.create(data);
    res.status(201).json("Dueño creado con éxito.")
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Obtener dueños */
exports.obtener = async (req, res) => {
  try {
    const owners = await models.Owners.findAll({attributes: ["id","name","telephone","email","identificactionnumber","address"],order: [['name', 'ASC']],raw: true});
    res.status(200).json(owners)
  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Actualizar dueño */
exports.actualizar = async (req, res) => {
  const {id} = req.params;
  let data = req.body; 
  try {
    await models.Owners.update(data, {where: { id }});    
    res.status(200).json("Dueño actualizado con éxito.");

  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};

/**Eliminar dueño */
exports.eliminar = async (req, res) => {
  const {id} = req.params;
  try {
    await models.Owners.destroy({where: { id }});    
    res.status(200).json("Dueño eliminado con éxito.");

  } catch (err) {
    res.status(500).send({
      message: err.message || errorDb,
    });
  }
};