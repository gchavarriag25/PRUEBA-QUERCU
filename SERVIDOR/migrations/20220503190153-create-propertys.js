'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Propertys', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      propertytypeid: {
        type: Sequelize.INTEGER,
        references:{
          model: "PropertyTypes",
          key: "id"
        }
      },
      ownerid: {
        type: Sequelize.INTEGER,
        references:{
          model: "Owners",
          key: "id"
        }
      },
      number: {
        type: Sequelize.STRING,
        allowNull: false
      },
      address: {
        type: Sequelize.STRING,
        allowNull: false
      },
      area: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      constructionarea: {
        type: Sequelize.DECIMAL
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Propertys');
  }
};