'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Propertys extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Propertys.init({
    propertytypeid: DataTypes.INTEGER,
    ownerid: DataTypes.INTEGER,
    number: DataTypes.STRING,
    address: DataTypes.STRING,
    area: DataTypes.DECIMAL,
    constructionarea: DataTypes.DECIMAL
  }, {
    sequelize,
    modelName: 'Propertys',
  });

  Propertys.getPropertys = () => {
    return sequelize.query(
      `select p.id, p.propertytypeid, p.ownerid, p.number as numberproperty, p.address, p.area, p.constructionarea, 
      pt.description, o.name, o.telephone, o.email, o.identificactionnumber  
      from "Propertys" p
      inner join "PropertyTypes" pt on pt.id = p.propertytypeid
      inner join "Owners" o on o.id = p.ownerid
      order by p.id`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  Propertys.getPropertyTypes = () => {
    return sequelize.query(
      `select id as value, description as label from "PropertyTypes"`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  Propertys.getOwners = () => {
    return sequelize.query(
      `select id as value, name as label from "Owners"`,
      { type: sequelize.QueryTypes.SELECT }
    );
  };

  return Propertys;
};