// import { useDispatch, useSelector } from "react-redux";
import Button from "../../components/inputs/Button";

export const PropertyTypesData = (
  info,
  deletePropertyType,
  getPropertyType
) => {
  const data = {
    columns: [
      {
        label: "Descipción",
        field: "description",
        width: 200,
      },
      {
        label: "Acción",
        field: "accion",
        width: 200,
      },
    ],
    rows:
      info.length > 0
        ? info.map((e) => ({
            description: e.description.toUpperCase(),
            accion: (
              <div className="d-flex">
                <Button
                  titulo={"Editar"}
                  handleClick={() => getPropertyType(e)}
                  icon={"bi bi-pencil-fill m-2"}
                />
                &nbsp;
                <Button
                  titulo={"Eliminar"}
                  handleClick={() => deletePropertyType(e.id)}
                  icon={"bi bi-trash-fill m-2"}
                />
              </div>
            ),
          }))
        : [],
  };
  return data;
};

export const OwnerData = (info, deleteOwner, getPropertyType) => {
  const data = {
    columns: [
      {
        label: "Nombre",
        field: "name",
        width: 100,
      },
      {
        label: "Telefono",
        field: "telephone",
        width: 100,
      },
      {
        label: "Correo Electronico",
        field: "email",
        width: 100,
      },
      {
        label: "Número de Identificación",
        field: "identificactionnumber",
        width: 100,
      },
      {
        label: "Dirección",
        field: "address",
        width: 100,
      },
      {
        label: "Acción",
        field: "accion",
        width: 200,
      },
    ],
    rows:
      info.length > 0
        ? info.map((e) => ({
            name: e.name.toUpperCase(),
            telephone: e.telephone,
            email: e.email,
            identificactionnumber: e.identificactionnumber,
            address: e.address.toUpperCase(),
            accion: (
              <div className="d-flex">
                <Button
                  titulo={"Editar"}
                  handleClick={() => getPropertyType(e)}
                  icon={"bi bi-pencil-fill m-2"}
                />
                &nbsp;
                <Button
                  titulo={"Eliminar"}
                  handleClick={() => deleteOwner(e.id)}
                  icon={"bi bi-trash-fill m-2"}
                />
              </div>
            ),
          }))
        : [],
  };
  return data;
};

export const PropertyData = (info, deleteOwner, getPropertyType) => {
  const data = {
    columns: [
      {
        label: "Num. Propiedad",
        field: "numberproperty",
        width: 100,
      },
      {
        label: "Tipo de Propiedad",
        field: "description",
        width: 100,
      },
      {
        label: "Dueño",
        field: "name",
        width: 100,
      },
      {
        label: "Dirección",
        field: "address",
        width: 100,
      },
      {
        label: "Área",
        field: "area",
        width: 100,
      },
      {
        label: "Área Construccion",
        field: "constructionarea",
        width: 100,
      },
      {
        label: "Acción",
        field: "accion",
        width: 200,
      },
    ],
    rows:
      info.length > 0
        ? info.map((e) => ({
            numberproperty: e.numberproperty,
            description: e.description,
            name: e.name.toUpperCase(),
            address: e.address.toUpperCase(),
            area: e.area,
            constructionarea: e.constructionarea,
            accion: (
              <div className="d-flex">
                <Button
                  titulo={"Editar"}
                  handleClick={() => getPropertyType(e)}
                  icon={"bi bi-pencil-fill m-2"}
                />
                &nbsp;
                <Button
                  titulo={"Eliminar"}
                  handleClick={() => deleteOwner(e.id)}
                  icon={"bi bi-trash-fill m-2"}
                />
              </div>
            ),
          }))
        : [],
  };
  return data;
};
