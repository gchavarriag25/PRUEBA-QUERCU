import React, { useState } from "react";
import { MDBDataTableV5 } from "mdbreact";
import '@fortawesome/fontawesome-free/css/all.min.css';

const Datatable = ({ datos }) => {

  const [checkbox1, setCheckbox1] = useState([]);

  const showLogs2 = (e) => {
    setCheckbox1(e);
  };

  return (
    <>
      <MDBDataTableV5
        responsive
        striped
        hover
        scrollY maxHeight='565px'
        entriesOptions={[100, 1000]}
        entries={1000}
        pagesAmount={4}
        data={datos}
        headCheckboxID="id6"
        bodyCheckboxID="checkboxes6"
        getValueCheckBox={(e) => {
          showLogs2(e);
        }}
        getValueAllCheckBoxes={(e) => {
          showLogs2(e);
        }}
        multipleCheckboxes
      />

      {checkbox1.length > 0 && (
        <p>
          {JSON.stringify(
            checkbox1.map((e) => {
              console.log(e);
              delete e.checkbox;
              return e;
            }) && checkbox1
          )}
        </p>
      )}
    </>
  );
};

export default Datatable;
