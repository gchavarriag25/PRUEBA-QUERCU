import React from "react";

const Button = ({ titulo, width, handleClick, icon }) => {
  return (
    <button
      className={"btn btn-primario btn-block mt-0"}
      type="button"
      onClick={handleClick}
    >
      <i className={icon}></i>
      {titulo}
    </button>
  );
};

export default Button;
