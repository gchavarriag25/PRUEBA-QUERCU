import React from "react";

const Text = ({id, name, placeholder, setChange, valor}) => {
  return (
    <input
      className="input-block"
      type="text"
      id={id}
      name={name}
      placeholder={placeholder}
      onChange={setChange}
      value={valor}
    />
  );
};

export default Text;
