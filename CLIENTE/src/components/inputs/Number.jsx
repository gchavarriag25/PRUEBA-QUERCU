import React from "react";

const Number = ({ id, name, placeholder, setChange, valor }) => {
  return (
    <input
      className="input-block"
      type="number"
      id={id}
      name={name}
      placeholder={placeholder}
      min={0}
      step={"any"}
      onChange={setChange}
      value={valor}
    />
  );
};

export default Number;
