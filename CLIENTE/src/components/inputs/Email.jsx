import React from "react";

const Email = ({ id, name, placeholder, setChange, valor }) => {
  return (
    <input
      className="input-block"
      type="email"
      id={id}
      name={name}
      placeholder={placeholder}
      onChange={setChange}
      value={valor}
    />
  );
};

export default Email;
