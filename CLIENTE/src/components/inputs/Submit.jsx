import React from "react";

const Submit = ({titulo, width}) => {
  return (
    <input
      type="submit"
      className={"btn btn-primario btn-block mt-0 w-" + width}
      value={titulo}
    />
  );
};

export default Submit;
