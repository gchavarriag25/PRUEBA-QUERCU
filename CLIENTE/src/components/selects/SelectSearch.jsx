import React from "react";
import CreatableSelect from "react-select/creatable";

const SelectSearch = ({ setChange, valor, opciones, width, disable, titulo }) => {
  return (
    <CreatableSelect
      className={"basic-single w-" + width}
      classNamePrefix="select"
      isClearable
      options={opciones}
      placeholder={titulo !== undefined ? titulo : "Seleccionar"}
      value={valor}
      isDisabled={disable ? disable : false}
      onChange={(item) => setChange(item)}
    />
  );
};

export default SelectSearch;
