import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <button
            type="button"
            className="navbar-toggler"
            data-bs-toggle="collapse"
            data-bs-target="#navbarCollapse"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarCollapse">
            <div className="navbar-nav">
              <NavLink to={"/"} className="nav-item nav-link active">
                Propiedad
              </NavLink>
              <NavLink to={"/property-type"} className="nav-item nav-link">
                Tipo Propiedad
              </NavLink>
              <NavLink to={"/owner"} className="nav-item nav-link">
                Dueño
              </NavLink>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
