import React from "react";
import { NavLink } from "react-router-dom";

const Navlink = ({ titulo, to, width, iconClassName }) => {
  return (
    <>
      <NavLink
        exact
        to={to}
        className={"btn btn-primario btn-block mt-0 w-" + width}
      >
        <i className={iconClassName}></i>
        {titulo}
      </NavLink>
    </>
  );
};

export default Navlink;
