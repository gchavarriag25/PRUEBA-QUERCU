import React, { useEffect } from "react";
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import Submit from "../../inputs/Submit";
import { useSelector } from "react-redux";
import Text from "../../inputs/Text";
import Number from "../../inputs/Number";
import Email from "../../inputs/Email";

const ModalAgregar = ({ owner, setOwner, setModal, edit, handleSubmit }) => {
  const { name, telephone, email, identificaction, address } = owner;

  const ownerEdit = useSelector((state) => state.owner.ownerEdit);

  useEffect(() => {
    if (ownerEdit !== null) {
      setOwner({
        name: ownerEdit.name,
        telephone: ownerEdit.telephone,
        email: ownerEdit.email,
        identificaction: ownerEdit.identificactionnumber,
        address: ownerEdit.address,
      });
    }
    // eslint-disable-next-line
  }, [ownerEdit]);

  const onChange = (e) => {
    setOwner({
      ...owner,
      [e.target.name]: e.target.value,
    });
  };

  const closeModal = () => {
    setModal(false);
  };

  return (
    <>
      <Modal isOpen={true} toggle={closeModal} size="lg">
        <ModalHeader
          close={
            <button className="close btn btn-dark" onClick={closeModal}>
              <i className="bi bi-x-lg"></i>
            </button>
          }
          toggle={closeModal}
        >
          <div>
            <h1 className={"m-4"}>{edit ? "Editar Dueño" : "Agregar Dueño"}</h1>
          </div>
        </ModalHeader>
        <ModalBody>
          <form className="" onSubmit={handleSubmit}>
            <div className="campo-form">
              <label htmlFor="name">Nombre:</label>
              <Text
                id={"name"}
                name={"name"}
                placeholder={"Ingrese un nombre"}
                setChange={onChange}
                valor={name}
              />
            </div>
            <div className="campo-form">
              <label htmlFor="telephone">Teléfono:</label>
              <Number
                id={"telephone"}
                name={"telephone"}
                placeholder={"Ingrese un número de teléfono"}
                setChange={onChange}
                valor={telephone}
              />
            </div>
            <div className="campo-form">
              <label htmlFor="email">Correo Electrónico:</label>
              <Email
                id={"email"}
                name={"email"}
                placeholder={"Ingrese un correo electrónico"}
                setChange={onChange}
                valor={email}
              />
            </div>
            <div className="campo-form">
              <label htmlFor="identificaction">Identificación:</label>
              <Text
                id={"identificaction"}
                name={"identificaction"}
                placeholder={"Ingrese un número de identificación"}
                setChange={onChange}
                valor={identificaction}
              />
            </div>
            <div className="campo-form">
              <label htmlFor="address">Dirección:</label>
              <Text
                id={"address"}
                name={"address"}
                placeholder={"Ingrese una dirección"}
                setChange={onChange}
                valor={address}
              />
            </div>
            <div className="campo-form">
              <Submit
                titulo={edit ? "Guardar Cambios" : "Agregar"}
                width={"100"}
              />
            </div>
          </form>
        </ModalBody>

        <ModalFooter></ModalFooter>
      </Modal>
    </>
  );
};

export default ModalAgregar;
