import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import Number from "../../inputs/Number";
import Submit from "../../inputs/Submit";
import Text from "../../inputs/Text";
import SelectSearch from "../../selects/SelectSearch";

const New = ({
  propiedad,
  setPropiedad,
  propertyType,
  setPropertyType,
  owner,
  setOwner,
  addProperty,
  edit,
}) => {
  const { number, address, area, constructionarea } = propiedad;

  const onChange = (e) => {
    setPropiedad({
      ...propiedad,
      [e.target.name]: e.target.value,
    });
  };

  const propertyEdit = useSelector((state) => state.property.propertyEdit);

  useEffect(() => {
    if (propertyEdit !== null) {
      setPropiedad({
        ...propiedad,
        number: propertyEdit.numberproperty,
        address: propertyEdit.address,
        area: propertyEdit.area,
        constructionarea: propertyEdit.constructionarea,
      });
      setPropertyType(
        propertyEdit.propertyTypeS ? propertyEdit.propertyTypeS : ""
      );
      setOwner(propertyEdit.ownerS ? propertyEdit.ownerS : "");
    }
    // eslint-disable-next-line
  }, [propertyEdit]);

  const propertyTypesOptions = useSelector(
    (state) => state.property.propertyTypes
  );
  const ownersOptions = useSelector((state) => state.property.owners);

  return (
    <>
      <form className="" onSubmit={addProperty}>
        <div className="campo-form">
          <label htmlFor="nombre">Tipo Propiedad:</label>
          <SelectSearch
            setChange={setPropertyType}
            valor={propertyType}
            opciones={propertyTypesOptions}
            width={"100"}
          />
        </div>
        <div className="campo-form">
          <label htmlFor="nombre">Dueño:</label>
          <SelectSearch
            setChange={setOwner}
            valor={owner}
            opciones={ownersOptions}
            width={"100"}
          />
        </div>
        <div className="campo-form">
          <label htmlFor="number">Número:</label>
          <Number
            id={"number"}
            name={"number"}
            placeholder={"Ingrese un número"}
            setChange={onChange}
            valor={number}
          />
        </div>
        <div className="campo-form">
          <label htmlFor="address">Dirección:</label>
          <Text
            id={"address"}
            name={"address"}
            placeholder={"Ingrese una dirección"}
            setChange={onChange}
            valor={address}
          />
        </div>
        <div className="campo-form">
          <label htmlFor="area">Área:</label>
          <Number
            id={"area"}
            name={"area"}
            placeholder={"0"}
            setChange={onChange}
            valor={area}
          />
        </div>
        <div className="campo-form">
          <label htmlFor="constructionarea">Área Construcción:</label>
          <Number
            id={"constructionarea"}
            name={"constructionarea"}
            placeholder={"0"}
            setChange={onChange}
            valor={constructionarea}
          />
        </div>
        <div className="campo-form">
          <Submit titulo={edit ? "Guardar Cambios" : "Agregar"} width={"100"} />
        </div>
      </form>
    </>
  );
};

export default New;
