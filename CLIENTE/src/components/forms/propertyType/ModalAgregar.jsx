import React, { useEffect } from "react";
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import Submit from "../../inputs/Submit";
import { useSelector } from "react-redux";
import Text from "../../inputs/Text";

const ModalAgregar = ({
  propertyType,
  setPropertyType,
  setModal,
  edit,
  handleSubmit,
}) => {
  const { description } = propertyType;

  const propertyTypeEdit = useSelector(
    (state) => state.propertyType.propertyTypeEdit
  );

  useEffect(() => {
    if (propertyTypeEdit !== null) {
      setPropertyType({
        description: propertyTypeEdit.description,
      });
    }
    // eslint-disable-next-line
  }, [propertyTypeEdit]);

  const onChange = (e) => {
    setPropertyType({
      ...propertyType,
      [e.target.name]: e.target.value,
    });
  };

  const closeModal = () => {
    setModal(false);
  };

  return (
    <>
      <Modal isOpen={true} toggle={closeModal} size="lg">
        <ModalHeader
          close={
            <button className="close btn btn-dark" onClick={closeModal}>
              <i className="bi bi-x-lg"></i>
            </button>
          }
          toggle={closeModal}
        >
          <div>
            <h1 className={"m-4"}>
              {edit
                ? "Editar Tipo de Propiedad"
                : "Agregar Tipo de Propiedad"}
            </h1>
          </div>
        </ModalHeader>
        <ModalBody>
          <form className="" onSubmit={handleSubmit}>
            <div className="campo-form">
              <label htmlFor="description">Descripción:</label>
              <Text
                id={"description"}
                name={"description"}
                placeholder={"Ingrese una descripción"}
                setChange={onChange}
                valor={description}
              />
            </div>
            <div className="campo-form">
              <Submit
                titulo={edit ? "Guardar Cambios" : "Agregar"}
                width={"100"}
              />
            </div>
          </form>
        </ModalBody>

        <ModalFooter></ModalFooter>
      </Modal>
    </>
  );
};

export default ModalAgregar;
