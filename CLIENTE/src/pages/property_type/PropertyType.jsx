import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addPropertyTypeAction,
  clearPropertyTypeAction,
  deletePropertyTypeAction,
  getPropertyTypeAction,
  getPropertyTypesAction,
} from "../../actions/propertyTypeAction";
import { PropertyTypesData } from "../../components/variables";
import Navbar from "../../components/layouts/Navbar";
import Datatable from "../../components/datatable/Datatable";
import Button from "../../components/inputs/Button";
import Spinner from "../../components/forms/Spinner/SpinnerCircular";
import ModalAgregar from "../../components/forms/propertyType/ModalAgregar";
import Swal from "sweetalert2";

const PropertyType = () => {
  const [modal, setModal] = useState(false);
  const [edit, setEdit] = useState(false);
  const [propertyType, setPropertyType] = useState({
    description: "",
  });

  const { description } = propertyType;

  const dispatch = useDispatch();

  const propertyTypesRequest = useSelector(
    (state) => state.propertyType.propertyTypes
  );
  const loading = useSelector((state) => state.propertyType.loading);
  const sending = useSelector((state) => state.propertyType.sending);
  const propertyTypeEdit = useSelector(
    (state) => state.propertyType.propertyTypeEdit
  );

  useEffect(() => {
    dispatch(getPropertyTypesAction());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (!modal) {
      dispatch(clearPropertyTypeAction());
      setEdit(false);
      setPropertyType({
        description: "",
      });
    }
    // eslint-disable-next-line
  }, [modal]);

  useEffect(() => {
    if (sending) {
      setModal(false);
      setPropertyType({
        description: "",
      });
      dispatch(getPropertyTypesAction());
    }
    // eslint-disable-next-line
  }, [sending]);

  /**Agrega y edita tipo de propiedad,  */
  const handleSubmit = (e) => {
    e.preventDefault();

    if (description === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo descripción es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }
    let id = null;
    if (propertyTypeEdit !== null) {
      id = propertyTypeEdit.id;
    }
    dispatch(addPropertyTypeAction({ description }, id));
  };

  /**Eliminar un tipo de propiedad */
  const deletePropertyType = (id) => {
    Swal.fire({
      title: "¿Estás seguro?",
      text: "Un tipo de propiedad que se elimina, no puede ser recuperado.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deletePropertyTypeAction(id));
      }
    });
  };

  /**Obtener informacion de la fila a editar */
  const getPropertyType = (row) => {
    if (row !== null || row !== undefined) {
      dispatch(getPropertyTypeAction(row));
      setModal(true);
      setEdit(true);
    }
  };

  const datatable = PropertyTypesData(
    propertyTypesRequest,
    deletePropertyType,
    getPropertyType,
  );

  return (
    <>
      <div className="row">
        <div>
          <Navbar />
        </div>
        <div className="col mx-auto">
          <div className="row">
            <h1 className="titulo-h1">Administración Tipos de Propiedades</h1>
            <div className="w-50 m-4 p-4">
              <Button
                titulo={"Agregar"}
                handleClick={() => setModal(true)}
                icon={"bi bi-plus-circle-fill m-2"}
              />
            </div>
            <div className="datatable-container col-11 mx-auto mt-4">
              {loading ? (
                <Spinner titilo={"Cargando"} />
              ) : (
                <Datatable datos={datatable} />
              )}
            </div>
          </div>
        </div>
        {modal && (
          <ModalAgregar
            propertyType={propertyType}
            setPropertyType={setPropertyType}
            setModal={setModal}
            edit={edit}
            handleSubmit={handleSubmit}
          />
        )}
      </div>
    </>
  );
};

export default PropertyType;
