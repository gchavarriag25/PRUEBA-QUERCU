import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";
import {
  addPropertyAction,
  getDataPropertyAction,
} from "../../actions/propertyAction";
import New from "../../components/forms/property/New";
import Navbar from "../../components/layouts/Navbar";

const PropertyNew = () => {
  const { id } = useParams();

  const [edit, setEdit] = useState(false);
  const [propertyType, setPropertyType] = useState("");
  const [owner, setOwner] = useState("");
  const [propiedad, setPropiedad] = useState({
    number: "",
    address: "",
    area: "",
    constructionarea: "",
  });

  const { number, address, area, constructionarea } = propiedad;

  const dispatch = useDispatch();

  const sending = useSelector((state) => state.property.sending);

  useEffect(() => {
    if (id !== undefined) {
      setEdit(true);
    } else {
      setEdit(false);
      clearForm();
    }
  }, [id]);

  useEffect(() => {
    dispatch(getDataPropertyAction());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (sending) {
      clearForm();
    }

    // eslint-disable-next-line
  }, [sending]);

  const clearForm = () => {
    setPropiedad({
      number: "",
      address: "",
      area: "",
      constructionarea: "",
    });
    setPropertyType("");
    setOwner("");
  };

  /**Registro propiedad */
  const addProperty = (e) => {
    e.preventDefault();

    if (number === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo número es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }

    if (address === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo dirección es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }

    if (area === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo área es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }

    let propertyObj = {
      propertytypeid: propertyType.value,
      ownerid: owner.value,
      number,
      address,
      area,
      constructionarea,
    };

    dispatch(addPropertyAction(propertyObj, edit, id));
  };

  return (
    <>
      <div className="row">
        <div>
          <Navbar />
        </div>
        <div className="col mx-auto">
          <div className="row">
            <h1 className="titulo-h1">
              {edit ? "Editar" : "Agregar"} Propiedad
            </h1>
            <div className="col-6 mx-auto">
              <New
                propiedad={propiedad}
                setPropiedad={setPropiedad}
                propertyType={propertyType}
                setPropertyType={setPropertyType}
                owner={owner}
                setOwner={setOwner}
                addProperty={addProperty}
                edit={edit}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PropertyNew;
