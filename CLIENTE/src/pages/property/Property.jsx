import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PropertyData } from "../../components/variables";
import Navbar from "../../components/layouts/Navbar";
import Datatable from "../../components/datatable/Datatable";
import Spinner from "../../components/forms/Spinner/SpinnerCircular";
import Swal from "sweetalert2";
import Navlink from "../../components/links/Navlink";
import {
  deletePropertyAction,
  getDataPropertyAction,
  getPropertyAction,
  getPropertysAction,
} from "../../actions/propertyAction";
import { useHistory } from "react-router-dom";

const Property = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const propertysRequest = useSelector((state) => state.property.propertys);
  const loading = useSelector((state) => state.propertyType.loading);

  const propertyTypesOptions = useSelector(
    (state) => state.property.propertyTypes
  );
  const ownersOptions = useSelector((state) => state.property.owners);

  useEffect(() => {
    dispatch(getPropertysAction());
    dispatch(getDataPropertyAction());
    // eslint-disable-next-line
  }, []);

  /**Eliminar propiedad */
  const deleteProperty = (id) => {
    Swal.fire({
      title: "¿Estás seguro?",
      text: "Una propiedad que se elimina, no puede ser recuperada.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deletePropertyAction(id));
      }
    });
  };

  /**Obtener informacion de la fila a editar */
  const getPropertyType = (row) => {
    let data = { ...row, propertyTypesOptions, ownersOptions };
    dispatch(getPropertyAction(data));
    history.push(`property-edit/${row.id}`);
  };

  const datatable = PropertyData(
    propertysRequest,
    deleteProperty,
    getPropertyType
  );

  return (
    <>
      <div className="row">
        <div>
          <Navbar />
        </div>
        <div className="col mx-auto">
          <div className="row">
            <h1 className="titulo-h1">Administración de Propiedades</h1>
            <div className="w-25 m-4 p-4">
              <Navlink
                titulo={"Agregar"}
                to={"/property-new"}
                width={"100"}
                iconClassName={"bi bi-plus-circle-fill m-2"}
              />
            </div>
            <div className="datatable-container col-11 mx-auto mt-4">
              {loading ? (
                <Spinner titilo={"Cargando"} />
              ) : (
                <Datatable datos={datatable} />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Property;
