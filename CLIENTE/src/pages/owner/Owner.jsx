import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { OwnerData } from "../../components/variables";
import Navbar from "../../components/layouts/Navbar";
import Datatable from "../../components/datatable/Datatable";
import Button from "../../components/inputs/Button";
import Spinner from "../../components/forms/Spinner/SpinnerCircular";
import ModalAgregar from "../../components/forms/owner/ModalAgregar";
import Swal from "sweetalert2";
import {
  addOwnerAction,
  clearOwnerAction,
  deleteOwnerAction,
  getOwnerAction,
  getOwnersAction,
} from "../../actions/ownerAction";

const Owner = () => {
  const [modal, setModal] = useState(false);
  const [edit, setEdit] = useState(false);
  const [owner, setOwner] = useState({
    name: "",
    telephone: "",
    email: "",
    identificaction: "",
    address: "",
  });

  const { name, telephone, email, identificaction, address } = owner;

  const dispatch = useDispatch();

  const ownersRequest = useSelector((state) => state.owner.owners);
  const loading = useSelector((state) => state.owner.loading);
  const sending = useSelector((state) => state.owner.sending);
  const ownerEdit = useSelector((state) => state.owner.ownerEdit);

  useEffect(() => {
    dispatch(getOwnersAction());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (!modal) {
      dispatch(clearOwnerAction());
      setEdit(false);
      setOwner({
        name: "",
        telephone: "",
        email: "",
        identificaction: "",
        address: "",
      });
    }
    // eslint-disable-next-line
  }, [modal]);

  useEffect(() => {
    if (sending) {
      setModal(false);
      setOwner({
        name: "",
        telephone: "",
        email: "",
        identificaction: "",
        address: "",
      });
      dispatch(getOwnersAction());
    }
    // eslint-disable-next-line
  }, [sending]);

  /**Agrega y edita dueño,  */
  const handleSubmit = (e) => {
    e.preventDefault();

    if (name === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo nombre es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }
    if (telephone === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo telefono es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }
    if (identificaction === "") {
      Swal.fire({
        icon: "error",
        title: "Verifique los datos.",
        text: "El campo número de identificacion es requerido.",
        confirmButtonText: "Aceptar",
      });
      return;
    }
    let ownerObj = {
      name,
      telephone,
      email,
      identificactionnumber: identificaction,
      address,
    };
    let id = null;
    if (ownerEdit !== null) {
      id = ownerEdit.id;
    }
    dispatch(addOwnerAction(ownerObj, id));
  };

  /**Eliminar dueño */
  const deleteOwner = (id) => {
    Swal.fire({
      title: "¿Estás seguro?",
      text: "Un dueño que se elimina, no puede ser recuperado.",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, eliminar!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(deleteOwnerAction(id));
      }
    });
  };

  /**Obtener informacion de la fila a editar */
  const getOwner = (row) => {
    if (row !== null || row !== undefined) {
      dispatch(getOwnerAction(row));
      setModal(true);
      setEdit(true);
    }
  };

  const datatable = OwnerData(ownersRequest, deleteOwner, getOwner);

  return (
    <>
      <div className="row">
        <div>
          <Navbar />
        </div>
        <div className="col mx-auto">
          <div className="row">
            <h1 className="titulo-h1">Administración Dueños</h1>
            <div className="w-50 m-4 p-4">
              <Button
                titulo={"Agregar"}
                handleClick={() => setModal(true)}
                icon={"bi bi-plus-circle-fill m-2"}
              />
            </div>
            <div className="datatable-container col-11 mx-auto mt-4">
              {loading ? (
                <Spinner titilo={"Cargando"} />
              ) : (
                <Datatable datos={datatable} />
              )}
            </div>
          </div>
        </div>
        {modal && (
          <ModalAgregar
            owner={owner}
            setOwner={setOwner}
            setModal={setModal}
            edit={edit}
            handleSubmit={handleSubmit}
          />
        )}
      </div>
    </>
  );
};

export default Owner;
