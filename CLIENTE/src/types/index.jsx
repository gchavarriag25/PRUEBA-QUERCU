/**PROPERTY TYPES */
export const PROPERTY_TYPE_START = "PROPERTY_TYPE_START";
export const PROPERTY_TYPE_ERROR = "PROPERTY_TYPE_ERROR";
export const PROPERTY_TYPE_SUCCESS = "PROPERTY_TYPE_SUCCESS";
export const PROPERTY_TYPE_ADD = "PROPERTY_TYPE_ADD";
export const PROPERTY_TYPE_DELETE = "PROPERTY_TYPE_DELETE";
export const PROPERTY_TYPE_GET = "PROPERTY_TYPE_GET";
export const PROPERTY_TYPE_CLEAR = "PROPERTY_TYPE_CLEAR";

/**OWNER */
export const OWNER_START = "OWNER_START";
export const OWNER_ERROR = "OWNER_ERROR";
export const OWNER_SUCCESS = "OWNER_SUCCESS";
export const OWNER_ADD = "OWNER_ADD";
export const OWNER_DELETE = "OWNER_DELETE";
export const OWNER_GET = "OWNER_GET";
export const OWNER_CLEAR = "OWNER_CLEAR";

/**PROPERTY */
export const PROPERTY_START = "PROPERTY_START";
export const PROPERTY_ERROR = "PROPERTY_ERROR";
export const PROPERTY_SUCCESS = "PROPERTY_SUCCESS";
export const PROPERTY_ADD = "PROPERTY_ADD";
export const PROPERTY_DELETE = "PROPERTY_DELETE";
export const PROPERTY_GET = "PROPERTY_GET";
export const PROPERTY_CLEAR = "PROPERTY_CLEAR";
export const PROPERTY_DATA = "PROPERTY_DATA";