import Swal from "sweetalert2";
import clienteAxios from "../config/axios";
import { OWNER_ADD, OWNER_CLEAR, OWNER_DELETE, OWNER_ERROR, OWNER_GET, OWNER_START, OWNER_SUCCESS } from "../types";

export const getOwnersAction = () => {
  return async (dispatch) => {
    dispatch(ownerStart());
    try {
      const response = await clienteAxios.get(`/owner`);
      dispatch(ownerSuccess(response.data));
    } catch (error) {
      dispatch(ownerError());
    }
  };
};

export const addOwnerAction = (data, id) => {
  return async (dispatch) => {
    dispatch(ownerStart());
    try {
      if (id === null) {
        await clienteAxios.post(`/owner`, data);
        Swal.fire({
          icon: "success",
          title: "Registro con éxito.",
          text: "Se ha registrado un nuevo dueño exitosamente.",
          confirmButtonText: "Aceptar",
        });
      } else {
        await clienteAxios.put(`/owner/${id}`, data);
        Swal.fire({
          icon: "success",
          title: "Actualizado con éxito.",
          text: "Se ha actualizado un dueño exitosamente.",
          confirmButtonText: "Aceptar",
        });
      }
      dispatch(addOwnerSuccess());
    } catch (error) {
      dispatch(ownerError());
    }
  };
};

export const deleteOwnerAction = (id) => {
  return async (dispatch) => {
    dispatch(ownerStart());
    try {
      await clienteAxios.delete(`/owner/${id}`);
      dispatch(deleteOwnerSuccess(id));
      Swal.fire({
        icon: "success",
        title: "Eliminado con éxito.",
        text: "Se ha eliminado un dueño exitosamente.",
        confirmButtonText: "Aceptar",
      });
    } catch (error) {
      dispatch(ownerError());
      Swal.fire({
        icon: "error",
        title: "No es posible eliminar el dueño.",
        text: "El dueño está vinculado a una propiedad.",
        confirmButtonText: "Aceptar",
      });
    }
  };
};

export const getOwnerAction = (data) => {
  return async (dispatch) => {
    dispatch(ownerStart());
    try {
      dispatch(getOwnerSuccess(data));
    } catch (error) {
      dispatch(ownerError());
    }
  };
};

export const ownerStart = () => ({
  type: OWNER_START,
});

export const ownerSuccess = (data) => ({
  type: OWNER_SUCCESS,
  payload: data,
});

export const ownerError = () => ({
  type: OWNER_ERROR,
});

export const addOwnerSuccess = () => ({
  type: OWNER_ADD,
});

export const deleteOwnerSuccess = (id) => ({
  type: OWNER_DELETE,
  payload: id,
});

export const getOwnerSuccess = (data) => ({
  type: OWNER_GET,
  payload: data,
});

export const clearOwnerAction = () => ({
  type: OWNER_CLEAR,
});