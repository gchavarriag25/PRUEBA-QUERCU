import Swal from "sweetalert2";
import clienteAxios from "../config/axios";
import {
  PROPERTY_ADD,
  PROPERTY_CLEAR,
  PROPERTY_DATA,
  PROPERTY_DELETE,
  PROPERTY_ERROR,
  PROPERTY_GET,
  PROPERTY_START,
  PROPERTY_SUCCESS,
} from "../types";

export const getPropertysAction = () => {
  return async (dispatch) => {
    dispatch(propertyStart());
    try {
      const response = await clienteAxios.get(`/property`);
      dispatch(propertySuccess(response.data));
    } catch (error) {
      dispatch(propertyError());
    }
  };
};

export const getDataPropertyAction = () => {
  return async (dispatch) => {
    dispatch(propertyStart());
    try {
      const response = await clienteAxios.get(`/property/data`);
      dispatch(propertyDataSuccess(response.data));
    } catch (error) {
      dispatch(propertyError());
    }
  };
};

export const addPropertyAction = (data, edit, id) => {
  return async (dispatch) => {
    dispatch(propertyStart());
    try {
      if (!edit) {
        await clienteAxios.post(`/property`, data);
        Swal.fire({
          icon: "success",
          title: "Registro con éxito.",
          text: "Se ha registrado una nueva propiedad exitosamente.",
          confirmButtonText: "Aceptar",
        });
      } else {
        await clienteAxios.put(`/property/${id}`, data);
        Swal.fire({
          icon: "success",
          title: "Actualizado con éxito.",
          text: "Se ha actualizado una propiedad exitosamente.",
          confirmButtonText: "Aceptar",
        });
      }
      dispatch(addPropertySuccess());
    } catch (error) {
      dispatch(propertyError());
    }
  };
};

export const deletePropertyAction = (id) => {
  return async (dispatch) => {
    dispatch(propertyStart());
    try {
      await clienteAxios.delete(`/property/${id}`);
      dispatch(deletePropertySuccess(id));
      Swal.fire({
        icon: "success",
        title: "Eliminado con éxito.",
        text: "Se ha eliminado una propiedad exitosamente.",
        confirmButtonText: "Aceptar",
      });
    } catch (error) {
      dispatch(propertyError());
      Swal.fire({
        icon: "error",
        title: "No es posible eliminar el tipo de propiedad.",
        text: "El tipo de propiedad está vinculado a una propiedad.",
        confirmButtonText: "Aceptar",
      });
    }
  };
};

export const getPropertyAction = (data) => {
  return async (dispatch) => {
    dispatch(propertyStart());
    try {
      const filterOwner = data.ownersOptions.filter(
        (o) => o.value === data.ownerid
      );
      const filterProperty = data.propertyTypesOptions.filter(
        (p) => p.value === data.propertytypeid
      );

      const ownerS = filterOwner[0];
      const propertyTypeS = filterProperty[0];

      const { propertyTypesOptions, ownersOptions, ...resto } = data;
      let datos = { ...resto, ownerS, propertyTypeS };

      dispatch(getPropertySuccess(datos));
    } catch (error) {
      dispatch(propertyError());
    }
  };
};

export const propertyStart = () => ({
  type: PROPERTY_START,
});

export const propertySuccess = (data) => ({
  type: PROPERTY_SUCCESS,
  payload: data,
});

export const propertyError = () => ({
  type: PROPERTY_ERROR,
});

export const propertyDataSuccess = (data) => ({
  type: PROPERTY_DATA,
  payload: data,
});

export const addPropertySuccess = () => ({
  type: PROPERTY_ADD,
});

export const deletePropertySuccess = (id) => ({
  type: PROPERTY_DELETE,
  payload: id,
});

export const getPropertySuccess = (data) => ({
  type: PROPERTY_GET,
  payload: data,
});

export const clearPropertyTypeAction = () => ({
  type: PROPERTY_CLEAR,
});
