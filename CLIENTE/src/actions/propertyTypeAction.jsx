import Swal from "sweetalert2";
import clienteAxios from "../config/axios";
import {
  PROPERTY_TYPE_ADD,
  PROPERTY_TYPE_CLEAR,
  PROPERTY_TYPE_DELETE,
  PROPERTY_TYPE_ERROR,
  PROPERTY_TYPE_GET,
  PROPERTY_TYPE_START,
  PROPERTY_TYPE_SUCCESS,
} from "../types";

export const getPropertyTypesAction = () => {
  return async (dispatch) => {
    dispatch(propertyTypeStart());
    try {
      const response = await clienteAxios.get(`/propertytype`);
      dispatch(propertyTypeSuccess(response.data));
    } catch (error) {
      dispatch(propertyTypeError());
    }
  };
};

export const addPropertyTypeAction = (data, id) => {
  return async (dispatch) => {
    dispatch(propertyTypeStart());
    try {
      if (id === null) {
        await clienteAxios.post(`/propertytype`, data);
        Swal.fire({
          icon: "success",
          title: "Registro con éxito.",
          text: "Se ha registrado un nuevo tipo de propiedad exitosamente.",
          confirmButtonText: "Aceptar",
        });
      } else {
        await clienteAxios.put(`/propertytype/${id}`, data);
        Swal.fire({
          icon: "success",
          title: "Actualizado con éxito.",
          text: "Se ha actualizado un tipo de propiedad exitosamente.",
          confirmButtonText: "Aceptar",
        });
      }
      dispatch(addPropertyTypeSuccess());
    } catch (error) {
      dispatch(propertyTypeError());
    }
  };
};

export const deletePropertyTypeAction = (id) => {
  return async (dispatch) => {
    dispatch(propertyTypeStart());
    try {
      await clienteAxios.delete(`/propertytype/${id}`);
      dispatch(deletePropertyTypeSuccess(id));
      Swal.fire({
        icon: "success",
        title: "Eliminado con éxito.",
        text: "Se ha eliminado un tipo de propiedad exitosamente.",
        confirmButtonText: "Aceptar",
      });
    } catch (error) {
      dispatch(propertyTypeError());
      Swal.fire({
        icon: "error",
        title: "No es posible eliminar el tipo de propiedad.",
        text: "El tipo de propiedad está vinculado a una propiedad.",
        confirmButtonText: "Aceptar",
      });
    }
  };
};

export const getPropertyTypeAction = (data) => {
  return async (dispatch) => {
    dispatch(propertyTypeStart());
    try {
      dispatch(getPropertyTypeSuccess(data));
    } catch (error) {
      dispatch(propertyTypeError());
    }
  };
};

export const propertyTypeStart = () => ({
  type: PROPERTY_TYPE_START,
});

export const propertyTypeSuccess = (data) => ({
  type: PROPERTY_TYPE_SUCCESS,
  payload: data,
});

export const propertyTypeError = () => ({
  type: PROPERTY_TYPE_ERROR,
});

export const addPropertyTypeSuccess = () => ({
  type: PROPERTY_TYPE_ADD,
});

export const deletePropertyTypeSuccess = (id) => ({
  type: PROPERTY_TYPE_DELETE,
  payload: id,
});

export const getPropertyTypeSuccess = (data) => ({
  type: PROPERTY_TYPE_GET,
  payload: data,
});

export const clearPropertyTypeAction = () => ({
  type: PROPERTY_TYPE_CLEAR,
});