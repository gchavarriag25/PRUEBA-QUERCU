import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
/** Redux */
import { Provider } from 'react-redux';
import store from './store';

import PropertyType from './pages/property_type/PropertyType';
import Owner from './pages/owner/Owner';
import Property from './pages/property/Property';
import PropertyNew from './pages/property/PropertyNew';


function App() {
  return (
    <Router>
      <Provider store = {store}>
        <div className="">
          <Switch>
            <Route exact path="/" component={Property}/>
            <Route exact path="/property-new" component={PropertyNew}/>
            <Route exact path="/property-edit/:id" component={PropertyNew}/>
            <Route exact path="/property-type" component={PropertyType}/>
            <Route exact path="/owner" component={Owner}/>
          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

export default App;
