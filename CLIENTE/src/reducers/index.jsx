import { combineReducers } from "redux";
import propertyTypeReducer from ".//propertyTypeReducer";
import ownerReducer from "./ownerReducer";
import propertyReducer from "./propertyReducer";

export default combineReducers({
  propertyType: propertyTypeReducer,
  owner: ownerReducer,
  property: propertyReducer
});
