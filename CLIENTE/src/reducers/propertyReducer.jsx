import {
  PROPERTY_ADD,
  PROPERTY_DATA,
  PROPERTY_DELETE,
  PROPERTY_ERROR,
  PROPERTY_GET,
  PROPERTY_START,
  PROPERTY_SUCCESS
} from "../types";

const initialState = {
  propertys: [],
  owners: [],
  propertyTypes: [],
  propertyEdit: null,
  error: false,
  loading: false,
  sending: false,
};

// eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case PROPERTY_START:
      return {
        ...state,
        loading: true,
        error: false,
        sending: false,
        propertyEdit: null,
      };
    case PROPERTY_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        sending: false,
        propertyEdit: null,
      };
    case PROPERTY_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        propertys: action.payload,
      };
    case PROPERTY_DATA:
      return {
        ...state,
        loading: false,
        error: false,
        owners: action.payload.owners,
        propertyTypes: action.payload.propertyType
      }
    case PROPERTY_ADD:
      return {
        ...state,
        loading: false,
        error: false,
        propertyEdit: null,
        sending: true,
      };
    case PROPERTY_DELETE:
      return {
        ...state,
        loading: false,
        error: false,
        propertys: state.propertys.filter(
          (p) => p.id !== action.payload
        ),
      };
    case PROPERTY_GET:
      return {
        ...state,
        loading: false,
        error: false,
        propertyEdit: action.payload,
      };
    // case PROPERTY_TYPE_CLEAR:
    //   return {
    //     ...state,
    //     loading: false,
    //     error: false,
    //     propertyTypeEdit: null,
    //   }
    default:
      return state;
  }
};
