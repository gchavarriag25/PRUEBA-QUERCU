import {
  PROPERTY_TYPE_ADD,
  PROPERTY_TYPE_CLEAR,
  PROPERTY_TYPE_DELETE,
  PROPERTY_TYPE_ERROR,
  PROPERTY_TYPE_GET,
  PROPERTY_TYPE_START,
  PROPERTY_TYPE_SUCCESS,
} from "../types";

const initialState = {
  propertyTypes: [],
  propertyTypeEdit: null,
  error: false,
  loading: false,
  sending: false,
};

// eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case PROPERTY_TYPE_START:
      return {
        ...state,
        loading: true,
        error: false,
        sending: false,
        propertyTypeEdit: null,
      };
    case PROPERTY_TYPE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        sending: false,
        propertyTypeEdit: null,
      };
    case PROPERTY_TYPE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        propertyTypes: action.payload,
      };
    case PROPERTY_TYPE_ADD:
      return {
        ...state,
        loading: false,
        error: false,
        propertyTypeEdit: null,
        sending: true,
      };
    case PROPERTY_TYPE_DELETE:
      return {
        ...state,
        loading: false,
        error: false,
        propertyTypes: state.propertyTypes.filter(
          (p) => p.id !== action.payload
        ),
      };
    case PROPERTY_TYPE_GET:
      return {
        ...state,
        loading: false,
        error: false,
        propertyTypeEdit: action.payload,
      };
    case PROPERTY_TYPE_CLEAR:
      return {
        ...state,
        loading: false,
        error: false,
        propertyTypeEdit: null,
      }
    default:
      return state;
  }
};
