import {
  OWNER_ADD,
  OWNER_CLEAR,
  OWNER_DELETE,
  OWNER_ERROR,
  OWNER_GET,
  OWNER_START,
  OWNER_SUCCESS,
} from "../types";

const initialState = {
  owners: [],
  ownerEdit: null,
  error: false,
  loading: false,
  sending: false,
};

// eslint-disable-next-line
export default (state = initialState, action) => {
  switch (action.type) {
    case OWNER_START:
      return {
        ...state,
        loading: true,
        error: false,
        sending: false,
        ownerEdit: null,
      };
    case OWNER_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        sending: false,
        ownerEdit: null,
      };
    case OWNER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        owners: action.payload,
      };
    case OWNER_ADD:
      return {
        ...state,
        loading: false,
        error: false,
        ownerEdit: null,
        sending: true,
      };
    case OWNER_DELETE:
      return {
        ...state,
        loading: false,
        error: false,
        owners: state.owners.filter((o) => o.id !== action.payload),
      };
    case OWNER_GET:
      return {
        ...state,
        loading: false,
        error: false,
        ownerEdit: action.payload,
      };
    case OWNER_CLEAR:
      return {
        ...state,
        loading: false,
        error: false,
        ownerEdit: null,
      };
    default:
      return state;
  }
};
